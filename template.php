<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 */

/**
 * Implements hook_preprocess_page().
 */
function wwuzen_healthcenter_preprocess_page(&$variables) {
  $theme_path = drupal_get_path('theme', 'wwuzen_healthcenter');
  $js_options = array(
    'group' => JS_LIBRARY,
    'every_page' => FALSE,
  );

  if(drupal_is_front_page()) {
    drupal_add_js($theme_path . '/js/wwu-date.min.js', $js_options);
    drupal_add_js($theme_path . '/js/wwu-format-tel.min.js', $js_options);
    drupal_add_js($theme_path . '/js/wwu-health-center.min.js', $js_options);
  }

  $now = new DateTime();
  $current_timezone_offset = $now->getOffset() * 1000;

  $regular_opening_time  = theme_get_setting('regular_opening_time');
  $thursday_opening_time = theme_get_setting('thursday_opening_time');
  $closing_time          = theme_get_setting('closing_time');

  $health_center = array(
    'open_hours_title'                => theme_get_setting('open_hours_title'),
    'closed_hours_title'              => theme_get_setting('closed_hours_title'),
    'hours_of_operation_message'      => theme_get_setting('hours_of_operation_message'),
    'regular_opening_time'            => array(
      'hours' => (int) $regular_opening_time,
      'minutes' => ((float) $regular_opening_time) * 60 % 60,
    ),
    'regular_opening_time_message'    => theme_get_setting('regular_opening_time_message'),
    'thursday_opening_time'           => array(
      'hours' => (int) $thursday_opening_time,
      'minutes' => ((float) $thursday_opening_time) * 60 % 60,
    ),
    'thursday_opening_time_message'   => theme_get_setting('thursday_opening_time_message'),
    'closing_time'                    => array(
      'hours' => (int) $closing_time,
      'minutes' => ((float) $closing_time) * 60 % 60,
    ),
    'delta_before_opening'            => theme_get_setting('delta_before_opening'),
    'phone_number'                    => theme_get_setting('phone_number'),
    'nurse_consultation_phone_number' => theme_get_setting('nurse_consultation_phone_number'),
    'holidays_and_breaks_url'         => theme_get_setting('holidays_and_breaks_url'),
    'holidays_and_breaks_message'     => theme_get_setting('holidays_and_breaks_message'),
    'is_holiday_or_break'             => FALSE,
    'current_timezone_offset'         => $current_timezone_offset,
  );

  drupal_add_js(array('HealthCenter' => $health_center), 'setting');
}
