<?php

// Plugin definition
$plugin = array(
  'title' => t('Clean 3/2 Column'),
  'category' => t('Western'),
  'icon' => 'clean-3-2-col.png',
  'theme' => 'clean-3-2-col',
  'css' => '../../css/panel-layouts/clean-3-2-col.css',
  'regions' => array(
    'box-left' => t('Featured Box Left'),
    'box-center' => t('Featured Box Center'),
    'box-right' => t('Featured Box Right'),
    'main-left-col' => t('Main Left Column'),
    'main-right-col' => t('Main Right Column')
  ),
);
