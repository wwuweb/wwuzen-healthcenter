<?php
/**
 * @file
 * Generic 1x3 and 1x2 layout.  Created for the Student Health Center site. 
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout.
 */
?>

<div class="featured-box-container-clean-3-2-col">
  <div class="featured-box box-left-clean-3-2-col" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['box-left']; ?>
  </div>
  <div class="featured-box box-center-clean-3-2-col" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['box-center']; ?>
  </div>
  <div class="featured-box box-right-clean-3-2-col" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['box-right']; ?>
  </div>
</div>

<div class="main-box-container-clean-3-2-col">
  <div class="main-left-clean-3-2-col" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['main-left-col']; ?>
  </div>
  <div class="main-right-clean-3-2-col" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
    <?php print $content['main-right-col']; ?>
  </div>
</div>



