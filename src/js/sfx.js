(function ($, Drupal, window, document, undefined) {

  'use strict';

  Drupal.behaviors.mainPageContactNumber = {

    attach: function (context, settings) {
      var $message = $('main ul.menu > li:first-child > a', context);
      var $phone_number = $('.same-day a', context);

      if ($message.length) {
        var jqxhr = $.ajax({
          url: settings.HealthCenter.holidays_and_breaks_url,
          timeout: 5000,
          dataType: 'json',
          cache: false
        });

        jqxhr.done(function (data) {
          settings.HealthCenter.is_holiday_or_break = (data.length > 0);
        });

        jqxhr.fail(function (jqxhr, textStatus, errorThrown) {
          console.error(errorThrown + '. Request failed to get holiday/break calendar data.');
          console.log(jqxhr.responseText);
        });

        jqxhr.always(function () {
          var date = wwu.Date.toTimezone(new Date(), settings.HealthCenter.current_timezone_offset);
          var contact_info = wwu.HealthCenter.getContactInfo(date, settings.HealthCenter);

          $message.html('<strong>' + contact_info.message_title + '</strong><br><small>' + contact_info.message + '</small><br>');
          $phone_number.attr('href', 'tel:' + contact_info.phone_number);
          wwu.formatTel($phone_number);
        });
      }
    }

  };

})(jQuery, Drupal, this, this.document);
