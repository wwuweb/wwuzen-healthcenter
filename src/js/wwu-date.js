var wwu = (function ($, wwu, window, document, undefined) {

  'use strict';

  function date_to_min(date) {
    return date.getHours() * wwu.Date.MIN_PER_H + date.getMinutes();
  }

  function date_to_timezone(date, offset) {
    return new Date(date_to_utc(date).getTime() + offset);
  }

  function date_to_utc(date) {
    return new Date(date.getTime() + min_to_msec(date.getTimezoneOffset()));
  }

  function h_to_min(hours) {
    return hours * wwu.Date.MIN_PER_H;
  }

  function min_to_msec(minutes) {
    return minutes * wwu.Date.SEC_PER_MIN * wwu.Date.MSEC_PER_SEC;
  }

  function msec_to_min(milliseconds) {
    return milliseconds / wwu.Date.MSEC_PER_SEC / wwu.Date.SEC_PER_MIN;
  }

  wwu.Date = {

    MIN_PER_H : 60,

    SEC_PER_MIN : 60,

    MSEC_PER_SEC : 1000,

    DAY_OF_WEEK : {
      monday : 1,
      tuesday : 2,
      wendesday : 3,
      thursday : 4,
      friday : 5,
      saturday : 6,
      sunday : 7
    },

    toMinutes : date_to_min,

    toTimezone : date_to_timezone,

    toUTC : date_to_utc,

    hToMin : h_to_min,

    minToMsec : min_to_msec,

    msecToMin : msec_to_min,

  };

  return wwu;

})(jQuery, wwu || {}, this, this.document);
