var wwu = (function ($, wwu, window, document, undefined) {

  'use strict';

  function format_tel(string) {
    var has_country_code = string.indexOf('+') === 0;
    var start = (has_country_code ? 1 : 0);
    var area_code = string.slice(start, start + 3);
    var prefix = string.slice(start + 3, start + 6);
    var line_number = string.slice(start + 6);

    return '(' + area_code + ') ' + prefix + '-' + line_number;
  }

  function append_tel(text) {
    var style = '<style>.same-day a:after { content: "' + text + '"} </style>';

    $('head').append(style);
  }

  /**
   * Parses sidebar phone link into a CSS :after content style.  Works for US
   * Phone numbers with or without a country code.  Will output an error if the
   * number is invalid.
   */
  wwu.formatTel = function (selector) {
    var tel = $(selector).attr('href').slice(4);

    if (/^(?:\+1)?\d{10}$/.test(tel)) {
      append_tel(format_tel(tel));
    }
    else {
      append_tel('Error: invalid phone number');
    }
  };

  return wwu;

})(jQuery, wwu || {}, this, this.document);
