/**
 * @file Provides the getContactInfo function to the health center home page.
 * @author Nigel Packer 2017
 *
 * @namespace wwu
 */
var wwu = (function ($, wwu, window, document, undefined) {

  'use strict';

  /**
   * Determine whether the current time is before hours on the current day.
   *
   * Returns false if the health center is not open on the current day.
   * @function is_before_hours
   * @param {Date} date
   * @param {Object} opening
   * @returns {boolean}
   * @private
   */
  function is_before_hours(date, opening) {
    return opening && (date < opening);
  }

  /**
   * Determine whether the current time is after hours on the current day.
   *
   * Returns false if the health center is not open on the current day.
   * @function is_after_hours
   * @param {Date} date
   * @param {Object} closing
   * @returns {boolean}
   * @private
   */
  function is_after_hours(date, closing) {
    return closing && (date >= closing);
  }

  /**
   * Determine if the current time is within open hours.
   * @function is_open_hours
   * @param {Date} date
   * @param {Object} opening
   * @param {Object} closing
   * @returns {boolean}
   * @private
   */
  function is_open_hours(date, opening, closing) {
    return opening && closing && (date >= opening) && (date < closing);
  }

  /**
   * Determine if the current time is within the given period before opening.
   * @function is_delta_before_opening
   * @param {Date} date
   * @param {Object} opening
   * @param {number} delta
   * @returns {boolean}
   * @private
   */
  function is_delta_before_opening(date, opening, delta) {
    var delta_before_opening = (opening ? wwu.Date.msecToMin(opening.getTime() - date.getTime()) : -1);

    return (delta_before_opening <= delta) && (delta_before_opening > 0);
  }

  /**
   * Determine whether the current day falls on a weekend.
   * @function is_weekend
   * @param {Date} date
   * @returns {boolean}
   * @private
   */
  function is_weekend(date) {
    switch(date.getDay()) {
      case wwu.Date.DAY_OF_WEEK.saturday:
      case wwu.Date.DAY_OF_WEEK.sunday:
        return true;

      default:
        return false;
    }
  }

  /**
   * Get the opening time of the health center.
   *
   * Returns null if the health center is not open on the current day of the
   * week. Does not take into account closure due to a holiday or break.
   * @function get_opening
   * @param {Date} date
   * @param {Object} settings
   * @returns {Object|null} The opening time date object or null if the health
   *   center is not open on the current day.
   * @private
   */
  function get_opening(date, settings) {
    switch(date.getDay()) {
      case wwu.Date.DAY_OF_WEEK.thursday:
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), settings.thursday_opening_time.hours, settings.thursday_opening_time.minutes);

      case wwu.Date.DAY_OF_WEEK.saturday:
      case wwu.Date.DAY_OF_WEEK.sunday:
        return null;

      default:
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), settings.regular_opening_time.hours, settings.regular_opening_time.minutes);
    }
  }

  /**
   * Get the closing time of the health center.
   *
   * Returns null if the health center is not open on the current day of the
   * week. Does not take into account closure due to a holiday or break.
   * @function get_closing
   * @param {Date} date
   * @param {Object} settings
   * @returns {Object|null} The closing time date object or null if the health
   *   center is not open on the current day.
   * @private
   */
  function get_closing(date, settings) {
    switch(date.getDay()) {
      case wwu.Date.DAY_OF_WEEK.saturday:
      case wwu.Date.DAY_OF_WEEK.sunday:
        return null;

      default:
        return new Date(date.getFullYear(), date.getMonth(), date.getDate(), settings.closing_time.hours, settings.closing_time.minutes);
    }
  }

  /**
   * Get the message to display before hours.
   * @function get_before_hours_message
   * @param {Date} date
   * @param {Object} settings
   * @returns {String}
   * @private
   */
  function get_before_hours_message(date, settings) {
    var message;

    switch(date.getDay()) {

      case wwu.Date.DAY_OF_WEEK.thursday:
        message = settings.thursday_opening_time_message;
        break;

      default:
        message = settings.regular_opening_time_message;
    }

    return message;
  }

  /**
   * Get the message to display after hours.
   * @function get_after_hours_message
   * @param {Date} date
   * @param {Object} settings
   * @returns {String}
   * @private
   */
  function get_after_hours_message(date, settings) {
    var message;

    switch(date.getDay()) {

      case wwu.Date.DAY_OF_WEEK.wednesday:
        message = settings.thursday_opening_time_message;
        break;

      case wwu.Date.DAY_OF_WEEK.friday:
        message = settings.regular_opening_time_message + ' on Monday';
        break;

      default:
        message = settings.regular_opening_time_message;
    }

    return message;
  }

  /**
   * Get the message to display on the weekend.
   * @function get_weekend_message
   * @param {Date} date
   * @param {Object} settings @see {@link getContactInfo}
   * @returns {String}
   * @private
   */
  function get_weekend_message(date, settings) {
    var message;

    switch(date.getDay()) {

      default:
        message = settings.regular_opening_time_message + ' on Monday';
    }

    return message;
  }

  /**
   * @module wwu/HealthCenter
   */
  wwu.HealthCenter = {

    /**
     * @typedef {Object} ContactInfo
     * @property {String} message_title - The title to display with the contact
     *   info.
     * @property {String} message - The contact info message to display.
     * @property {String} phone_number - The contact phone number to display.
     */

    /**
     * Return the contact info for the Student Health Center based on the
     * current date.
     * @function getContactInfo
     * @param {Object} settings - Settings required to build the contact info.
     * @param {String} settings.open_hours_title - The title to display when the
     *   health center is open.
     * @param {String} settings.closed_hours_title - The title to display when
     *   the health center is closed.
     * @param {String} settings.hours_of_operation_message - The message to
     *   display during normal operating hours.
     * @param {Object} settings.regular_opening_time - The regular opening time.
     * @param {String} settings.regular_opening_time.hours - The hours component
     *   of the opening time as a 24-hour value.
     * @param {String} settings.regular_opening_time.minutes - The minutes
     *   component of the opening time as a 24-hour value.
     * @param {Object} settings.thursday_opening_time - The opening time of the
     *   health center on Thursdays.
     * @param {String} settings.thursday_opening_time.hours - The hours
     *   component of the thursday opening time as a 24-hour value.
     * @param {String} settings.thursday_opening_time.minutes - The minutes
     *   componen of the thursday opening time as a 24-hour value.
     * @param {String} settings.delta_before_opening - The number of minutes
     *   before the opening time considered "before opening".
     * @param {String} settings.phone_number - The phone number as an
     *   unformatted string.
     * @param {String} settings.nurse_consultation_phone_number - The nurse
     *   consultation phone number as an unformatted string.
     * @param {String} settings.holidays_and_breaks_message - The message to
     *   display during holidays and breaks.
     * @param {boolean} settings.is_holiday_or_break - Whether the current date
     *   falls on a holiday or break.
     * @returns {ContactInfo}
     * @public
     */
    getContactInfo : function (date, settings) {
      var contact_info = {};
      var opening = get_opening(date, settings);
      var closing = get_closing(date, settings);

      if (settings.is_holiday_or_break) {
        contact_info.message_title = settings.closed_hours_title;
        contact_info.message = settings.holidays_and_breaks_message;
        contact_info.phone_number = settings.nurse_consultation_phone_number;
      }
      else if (is_weekend(date)) {
        contact_info.message_title = settings.closed_hours_title;
        contact_info.message = get_weekend_message(date, settings);
        contact_info.phone_number = settings.nurse_consultation_phone_number;
      }
      else if (is_after_hours(date, closing)) {
        contact_info.message_title = settings.closed_hours_title;
        contact_info.message = get_after_hours_message(date, settings);
        contact_info.phone_number = settings.nurse_consultation_phone_number;
      }
      else if (is_before_hours(date, opening) && !is_delta_before_opening(date, opening, settings.delta_before_opening)) {
        contact_info.message_title = settings.closed_hours_title;
        contact_info.message = get_before_hours_message(date, settings);
        contact_info.phone_number = settings.nurse_consultation_phone_number;
      }
      else if (is_open_hours(date, opening, closing) || is_delta_before_opening(date, opening, settings.delta_before_opening)) {
        contact_info.message_title = settings.open_hours_title;
        contact_info.message = settings.hours_of_operation_message;
        contact_info.phone_number = settings.phone_number;
      }
      else {
        contact_info.message_title = settings.closed_hours_title;
        contact_info.message = settings.hours_of_operation_message;
        contact_info.phone_number = settings.nurse_consultation_phone_number;
      }

      return contact_info;
    }

  };

  return wwu;

})(jQuery, wwu || {}, this, this.document);
