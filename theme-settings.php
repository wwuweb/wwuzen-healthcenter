<?php
/**
 * @file
 * Customize the Apearance settings page for this theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function wwuzen_healthcenter_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL) {

  $form['contact'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Info'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['contact']['open_hours_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title during open hours'),
    '#default_value' => theme_get_setting('open_hours_title'),
    '#required'      => TRUE,
  );

  $form['contact']['closed_hours_title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title during closed hours'),
    '#default_value' => theme_get_setting('closed_hours_title'),
    '#required'      => TRUE,
  );

  $form['contact']['hours_of_operation_message'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Hours of operation'),
    '#default_value' => theme_get_setting('hours_of_operation_message'),
    '#required'      => TRUE,
  );

  $form['contact']['regular_opening_time'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Regular opening time'),
    '#description'   => t('In hours, expressed as a decimal number (e.g. 8:30am => 8.5)'),
    '#default_value' => theme_get_setting('regular_opening_time'),
    '#required'      => TRUE,
  );

  $form['contact']['regular_opening_time_message'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Regular opening time message'),
    '#default_value' => theme_get_setting('regular_opening_time_message'),
    '#required'      => TRUE,
  );

  $form['contact']['thursday_opening_time'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Thursday opening time'),
    '#description'   => t('In hours, expressed as a decimal number (e.g. 8:30am => 8.5)'),
    '#default_value' => theme_get_setting('thursday_opening_time'),
    '#required'      => TRUE,
  );

  $form['contact']['thursday_opening_time_message'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Thursday opening time message'),
    '#default_value' => theme_get_setting('thursday_opening_time_message'),
    '#required'      => TRUE,
  );

  $form['contact']['delta_before_opening'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Time before opening'),
    '#description'   => t('Amount of time before opening in minutes to take down the nurse consultation number'),
    '#default_value' => theme_get_setting('delta_before_opening'),
    '#required'      => TRUE,
  );

  $form['contact']['closing_time'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Closing time'),
    '#description'   => t('In hours, expressed as a decimal number (e.g. 5:00pm => 16)'),
    '#default_value' => theme_get_setting('closing_time'),
    '#required'      => TRUE,
  );

  $form['contact']['phone_number'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Health Center Phone Number'),
    '#description'   => t('The full phone number without formatting (e.g. 5555555555)'),
    '#default_value' => theme_get_setting('phone_number'),
    '#required'      => TRUE,
  );

  $form['contact']['nurse_consultation_phone_number'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Nurse consultation phone number'),
    '#description'   => t('The full phone number without formatting (e.g. 5555555555)'),
    '#default_value' => theme_get_setting('nurse_consultation_phone_number'),
    '#required'      => TRUE,
  );

  $form['contact']['holidays_and_breaks_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Query URL to check if it is a holidy or break'),
    '#default_value' => theme_get_setting('holidays_and_breaks_url'),
    '#required'      => TRUE,
  );

  $form['contact']['holidays_and_breaks_message'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Message to display during holidays and breaks'),
    '#default_value' => theme_get_setting('holidays_and_breaks_message'),
    '#required'      => TRUE,
  );

  if (isset($form_id)) {
    return;
  }

}
